FROM openjdk:17

ENV PORT 3030
EXPOSE $PORT
#if using windows there should be ARG DIR=build/libs/*.jar
#the reason why this line is different from windows one
#it's because the linux is required the derictory that ends with "/"
ARG DIR=build/libs/

COPY $DIR beltweet.jar

ENTRYPOINT java -jar beltweet.jar