CREATE TYPE country_enum AS ENUM(
    'NONE',
    'BELARUS', 'RUSSIA',
    'UKRAINE', 'KAZAKHSTAN',
    'ROMANIA', 'LITHUANIA',
    'USA', 'EGYPT',
    'CHINA', 'SOUTH KOREA'
);

CREATE TYPE city_enum AS ENUM(
    'NONE',
    'MINSK', 'MOSCOW',
    'KIEV', 'ASTANA',
    'BUCHAREST', 'VILNUS',
    'WASHINGTON', 'KAIR',
    'PEKIN', 'SEOUL'
);

CREATE TABLE IF NOT EXISTS current_location(
  id  UUID DEFAULT md5(random()::TEXT || clock_timestamp()::TEXT)::UUID NOT NULL UNIQUE,
  country country_enum DEFAULT 'NONE',
  city city_enum DEFAULT 'NONE'
);
--todo drop name everywhere
CREATE TABLE IF NOT EXISTS picture(
    id UUID DEFAULT md5(random()::TEXT || clock_timestamp()::TEXT)::UUID NOT NULL UNIQUE,
    name VARCHAR(255) NOT NULL,
    fpath VARCHAR(255) NOT NULL UNIQUE,
    is_default BOOLEAN DEFAULT TRUE,
    date_of_upload TIMESTAMP DEFAULT current_timestamp
);

CREATE INDEX ON picture(fpath);
CREATE INDEX ON picture(name);

CREATE TABLE IF NOT EXISTS user_information(
  id UUID DEFAULT md5(random()::TEXT || clock_timestamp()::TEXT)::UUID NOT NULL UNIQUE,
  current_location_id UUID NOT NULL UNIQUE,
  is_empty BOOLEAN DEFAULT TRUE, --todo add check case when description is null then true end
  description VARCHAR(255) DEFAULT 'user wont said something about him self',
  FOREIGN KEY (current_location_id) REFERENCES current_location(id)
);

CREATE TABLE IF NOT EXISTS profile(
  id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  user_id VARCHAR(255) NOT NULL UNIQUE,
  picture_id UUID UNIQUE,
  user_information_id UUID UNIQUE,
  FOREIGN KEY (user_id) REFERENCES user_entity(id),
  FOREIGN KEY (picture_id) REFERENCES picture(id),
  FOREIGN KEY (user_information_id) REFERENCES user_information(id)
);

CREATE TABLE IF NOT EXISTS post_data(
    id UUID DEFAULT md5(random()::TEXT || clock_timestamp()::TEXT)::UUID NOT NULL UNIQUE,
    description VARCHAR(255) CHECK(LENGTH(description) >= 10)
);

CREATE TABLE IF NOT EXISTS post(
  id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  user_id VARCHAR(255) NOT NULL,
  post_data_id UUID UNIQUE,
  posted_in timestamp DEFAULT current_timestamp,
  FOREIGN KEY (user_id) REFERENCES user_entity(id),
  FOREIGN KEY (post_data_id) REFERENCES post_data(id)
);
