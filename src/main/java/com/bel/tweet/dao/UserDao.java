package com.bel.tweet.dao;

import com.bel.tweet.model.entity.User;

public interface UserDao {

    User findUserById(String id);

    User findUserByUsername(String username);
}
