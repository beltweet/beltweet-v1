package com.bel.tweet.dao.impl;

import com.bel.tweet.config.util.AwsProperties;
import com.bel.tweet.dao.ProfileDao;
import com.bel.tweet.model.entity.Picture;
import com.bel.tweet.model.entity.Profile;
import com.bel.tweet.model.enumeration.Country;
import com.bel.tweet.model.mapper.ProfileMapper;
import com.bel.tweet.model.mapper.UpdatedProfileMapper;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Repository
@AllArgsConstructor
public class ProfileDaoImpl implements ProfileDao {

    private final JdbcTemplate jdbcTemplate;
    private final AwsProperties awsProperties;

    @Override //todo do something with wait as var's
    public void save(@NonNull Profile profile) {
        var sql = """
                WITH var_current_location_id\040
                AS(INSERT INTO current_location(country, city) VALUES (NULL, NULL) RETURNING id),\040
                var_picture_id\040
                AS(SELECT id FROM picture WHERE fpath = ?),\040
                var_user_information_id\040
                AS(INSERT INTO user_information(current_location_id)\040
                VALUES ((SELECT * FROM var_current_location_id)) RETURNING id)\040
                INSERT INTO profile(user_id, user_information_id, picture_id)\040
                VALUES (?, (SELECT * FROM var_user_information_id), (SELECT * FROM var_picture_id))
                """;

        var userId = profile.getUser().getId();

        jdbcTemplate.update(sql, awsProperties.getDefaultImagePath(), userId);
    }

    @Override //todo return role of user
    public Profile findProfileById(@NotBlank long id) { //todo add picture_id
        var sql = """ 
                SELECT (var_profile.id) AS profile_id, (var_user.id) AS user_id, var_user.username, var_user.email, var_user.first_name, var_user.last_name,\040
                (var_user_information.id) AS user_information_id, var_user_information.description, var_user_information.is_empty,\040
                (var_current_location.id) AS current_location_id, var_current_location.city, var_current_location.country\040
                FROM profile var_profile\040
                JOIN user_entity var_user ON var_user.id = var_profile.user_id\040
                JOIN user_information var_user_information ON var_user_information.id = var_profile.user_information_id\040
                JOIN current_location var_current_location ON var_current_location.id = var_user_information.current_location_id\040
                WHERE var_profile.id = ?
                """;

        return jdbcTemplate.queryForObject(sql, new ProfileMapper(), id);
    }

    @Override
    public Profile findProfileByUserId(@NonNull String id, @NonNull Profile profile) {
        var sql = """
                SELECT (var_profile.user_information_id) AS user_information_id, (var_current_location.id) AS current_location_id\040
                FROM profile var_profile\040
                JOIN current_location var_current_location ON var_current_location.id =\040
                (SELECT current_location_id FROM user_information WHERE id = var_profile.user_information_id)\040
                WHERE user_id = ?
                """;

        return jdbcTemplate.queryForObject(sql, new UpdatedProfileMapper(profile), id);
    }

    @Override
    public void update(@NonNull Profile profile) {
        var currentLocationSql = """
                UPDATE current_location SET country = COALESCE(?::country_enum),\040
                city = COALESCE(?::city_enum) WHERE id = ?
                """;

        var userInformationSql = """
                UPDATE user_information SET description = COALESCE(?),\040
                is_empty = CASE WHEN ? IS NOT NULL AND is_empty = TRUE THEN FALSE END WHERE id = ?
                """;

        var userInformation = profile.getUserInformation();

        var currentLocation = userInformation.getCurrentLocation();

        jdbcTemplate.update(currentLocationSql,
                currentLocation.getCountry().toString(), currentLocation.getCity().toString(), currentLocation.getId());

        jdbcTemplate.update(userInformationSql,
                userInformation.getDescription(), userInformation.getDescription(), userInformation.getId());
    }

    @Override
    public Country findProfileCountryByUserId(@NonNull String id) {
        var sql = """
                SELECT var_current_location.country FROM profile var_profile\040
                JOIN user_information var_user_information ON var_user_information.id = var_profile.user_information_id\040
                JOIN current_location var_current_location ON var_current_location.id = var_user_information.current_location_id\040
                WHERE var_profile.user_id = ?
                """;

        return jdbcTemplate.queryForObject(sql, Country.class, id);
    }

    @Override
    public void update(String id, String fileName, String key) {
        var sql = """
                UPDATE picture SET name = ?, fpath = ? WHERE id = ?
                """;

        jdbcTemplate.update(sql, fileName, key, UUID.fromString(id));
    }

    @Override
    public long findProfileIdByUserId(@NotNull String id) {
        var sql = """
                SELECT id FROM profile WHERE user_id = ?
                """;

        var profileId = jdbcTemplate.queryForObject(sql, Long.TYPE, id);

        assert profileId != null;

        return profileId;
    }

    @Override
    public String findPictureIdByProfileId(long id) {
        var sql = """
                SELECT picture_id FROM profile WHERE id = ?
                """;

        return jdbcTemplate.queryForObject(sql, String.class, id);
    }

    @Override
    public boolean findIfPictureIsDefaultByUserId(String id) {
        var sql = """
                SELECT var_picture.is_default FROM profile var_profile\040
                JOIN picture var_picture ON var_picture.id = var_profile.picture_id WHERE var_profile.user_id = ?;
                """;

        var isDefault = jdbcTemplate.queryForObject(sql, Boolean.TYPE, id);

        assert isDefault != null;

        return isDefault;
    }

    @Override
    public void save(String id, String fileName, String path) {
        var sql = """
                WITH var_profile_id AS(SELECT id FROM profile WHERE user_id = ?),\040
                var_new_picture_id AS(INSERT INTO picture(name, fpath, is_default) VALUES (?, ?, FALSE) RETURNING id)\040
                UPDATE profile SET picture_id = (SELECT * FROM var_new_picture_id) WHERE id = (SELECT * FROM var_profile_id)
                """;

        jdbcTemplate.update(sql, id, fileName, path);
    }

    @Override
    public Picture findPictureByUserId(String id) {
        var sql = """
                SELECT var_picture.id AS id, var_picture.name AS name, var_picture.fpath AS path, is_default AS isDefault, date_of_upload AS dateOfUpload FROM profile var_profile\040
                JOIN picture var_picture ON var_picture.id = var_profile.picture_id WHERE var_profile.user_id = ?;
                """;

        return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(Picture.class), id);
    }
}
