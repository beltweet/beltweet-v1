package com.bel.tweet.dao.impl;

import com.bel.tweet.dao.UserDao;
import com.bel.tweet.model.entity.User;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
@AllArgsConstructor
public class UserDaoImpl implements UserDao {

    private final JdbcTemplate jdbcTemplate;

    @Override
    public User findUserById(@NonNull String id) {
        var sql = """
                SELECT id, email, first_name, last_name, username, enabled\040
                FROM user_entity WHERE id = ?
                """;

        return jdbcTemplate.queryForObject(sql,
                new BeanPropertyRowMapper<>(User.class), id);
    }

    @Override
    public User findUserByUsername(@NonNull String username) {
        var sql = """
                SELECT id, email, first_name, last_name, username, enabled\040
                FROM user_entity WHERE username = ?
                """;

        return jdbcTemplate.queryForObject(sql,
                new BeanPropertyRowMapper<>(User.class), username);
    }
}
