package com.bel.tweet.dao;

import com.bel.tweet.model.entity.PostRating;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRatingDao extends CassandraRepository<PostRating, String> {

    @Query("SELECT COUNT(*) FROM posts.post_rating WHERE rating = :rating AND post_id = :postId")
    int findCountedRatingByRatingName(@Param("rating") String rating, @Param("postId") String postId);
}
