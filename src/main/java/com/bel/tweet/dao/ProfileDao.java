package com.bel.tweet.dao;

import com.bel.tweet.model.entity.Picture;
import com.bel.tweet.model.entity.Profile;
import com.bel.tweet.model.enumeration.Country;

public interface ProfileDao {

    void save(Profile profile);

    Profile findProfileById(long id);

    Profile findProfileByUserId(String id, Profile profile);

    void update(Profile profile);

    Country findProfileCountryByUserId(String id);

    void update(String id, String fileName, String key);

    long findProfileIdByUserId(String id);

    String findPictureIdByProfileId(long id);

    Picture findPictureByUserId(String id);

    boolean findIfPictureIsDefaultByUserId(String id);

    void save(String id, String fileName, String path);
}
