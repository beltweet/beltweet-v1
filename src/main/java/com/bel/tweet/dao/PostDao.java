package com.bel.tweet.dao;

import com.bel.tweet.model.entity.Post;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface PostDao extends CassandraRepository<Post, String> {

    @Query("SELECT id, owner_id, description, posted_in FROM posts.post_data WHERE owner_id = :ownerId")
    List<Post> findAllPostsByUserId(@Param("ownerId") UUID userId);
}
