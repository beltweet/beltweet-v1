package com.bel.tweet.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Picture {
    private UUID id;

    private String name;

    private String path;

    private boolean isDefault;

    private LocalDateTime dateOfUpload;
}
