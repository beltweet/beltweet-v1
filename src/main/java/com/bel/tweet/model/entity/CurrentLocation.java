package com.bel.tweet.model.entity;

import com.bel.tweet.model.enumeration.City;
import com.bel.tweet.model.enumeration.Country;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CurrentLocation {

    private UUID id;

    private Country country;

    private City city;
}


