package com.bel.tweet.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserInformation {

    private UUID id;

    private CurrentLocation currentLocation;

    private boolean isEmpty;

    private String description;
}
