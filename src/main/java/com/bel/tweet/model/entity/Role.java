package com.bel.tweet.model.entity;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class Role{
    private UUID id;

    private String name;
}
