package com.bel.tweet.model.entity;

import com.bel.tweet.model.enumeration.Country;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.CassandraType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "post_data")
public class Post {

    @Builder.Default
    private UUID id = UUID.randomUUID();

    @Column(value = "owner_id")
    private UUID userOwnerId; //todo change to ownerId

    private String description;

    @Column(value = "country_of_owner")
    @CassandraType(type = CassandraType.Name.TEXT)
    private Country countryOfOwner;

    @Transient
    @Builder.Default
    private Map<String, Integer> ratings = new HashMap<>();

    @PrimaryKeyColumn(
            name = "posted_in",
            type = PrimaryKeyType.PARTITIONED,
            ordering = Ordering.DESCENDING
    )
    private LocalDateTime postedIn;
}
