package com.bel.tweet.model.entity;

import com.bel.tweet.model.enumeration.Rating;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.CassandraType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "post_rating")
public class PostRating {

    @PrimaryKeyColumn(
            name = "post_id",
            type = PrimaryKeyType.PARTITIONED
    )
    private String postId;

    @PrimaryKeyColumn(
            name = "user_id",
            type = PrimaryKeyType.CLUSTERED
    )
    private String userId;

    @CassandraType(type = CassandraType.Name.TEXT)
    private Rating rating;
}
