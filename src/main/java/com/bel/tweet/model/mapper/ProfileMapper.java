package com.bel.tweet.model.mapper;

import com.bel.tweet.model.entity.CurrentLocation;
import com.bel.tweet.model.entity.Profile;
import com.bel.tweet.model.entity.User;
import com.bel.tweet.model.entity.UserInformation;
import com.bel.tweet.model.enumeration.City;
import com.bel.tweet.model.enumeration.Country;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Objects;
import java.util.UUID;


public class ProfileMapper implements RowMapper<Profile> {

    @Nullable
    @Override
    public Profile mapRow(ResultSet rs, int rowNum) throws SQLException {
        var user = User.builder()
                .id(rs.getString("user_id"))
                .username(rs.getString("username"))
                .firstName(rs.getString("first_name"))
                .lastName(rs.getString("last_name"))
                .email(rs.getString("email"))
                .roles(Collections.emptyList()) //todo set roles
                .build();

        var currentLocation = CurrentLocation.builder()
                .id(UUID.fromString(rs.getString("current_location_id")))
                .country(getCountry(rs))
                .city(getCity(rs))
                .build();

        var userInformation = UserInformation.builder()
                .id(UUID.fromString(rs.getString("user_information_id")))
                .currentLocation(currentLocation)
                .description(rs.getString("description"))
                .isEmpty(rs.getBoolean("is_empty"))
                .build();

        return Profile.builder()
                .id(rs.getLong("profile_id"))
                .user(user)
                .userInformation(userInformation)
                .build();
    }

    //todo made instead of this lambda func's
    private Country getCountry(ResultSet rs) throws SQLException {
        var country = rs.getString("country");

        return country != null ? Country.valueOf(country) : Country.NONE;
    }

    private City getCity(ResultSet rs) throws SQLException {
        var city = rs.getString("city");

        return city != null ? City.valueOf(city) : City.NONE;
    }
}
