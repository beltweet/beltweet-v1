package com.bel.tweet.model.mapper;

import com.bel.tweet.model.entity.Profile;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

@AllArgsConstructor
public class UpdatedProfileMapper implements RowMapper<Profile> {

    private final Profile profile;

    @Nullable
    @Override
    public Profile mapRow(ResultSet rs, int rowNum) throws SQLException {
        profile
                .getUserInformation()
                .getCurrentLocation()
                .setId(UUID.fromString(rs.getString("current_location_id")));

        profile
                .getUserInformation()
                .setId(UUID.fromString(rs.getString("user_information_id")));

        return profile;
    }
}
