package com.bel.tweet.model.converter;

import com.bel.tweet.model.dto.RoleDto;
import com.bel.tweet.model.entity.Role;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class RoleToRoleDtoConverter implements Converter<Role, RoleDto> {

    private final ModelMapper modelMapper;

    @Override
    public RoleDto convert(Role source) {
        return modelMapper.map(source, RoleDto.class);
    }
}
