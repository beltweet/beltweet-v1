package com.bel.tweet.model.converter;

import com.bel.tweet.model.dto.PictureDto;
import com.bel.tweet.model.entity.Picture;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class PictureDtoToPictureConverter implements Converter<PictureDto, Picture> {

    private final ModelMapper modelMapper;

    @Override
    public Picture convert(PictureDto source) {
        return modelMapper.map(source, Picture.class);
    }
}
