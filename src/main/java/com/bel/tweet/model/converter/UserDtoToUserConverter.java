package com.bel.tweet.model.converter;

import com.bel.tweet.model.dto.UserDto;
import com.bel.tweet.model.entity.User;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class UserDtoToUserConverter implements Converter<UserDto, User> {

    private final ModelMapper modelMapper;

    @Override
    public User convert(UserDto source) {
        return modelMapper.map(source, User.class);
    }
}
