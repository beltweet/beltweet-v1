package com.bel.tweet.model.converter;

import com.bel.tweet.model.dto.PostDto;
import com.bel.tweet.model.entity.Post;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class PostDtoToPostConverter implements Converter<PostDto, Post> {

    private final ModelMapper modelMapper;

    @Override
    public Post convert(PostDto source) {
        return modelMapper.map(source, Post.class);
    }
}
