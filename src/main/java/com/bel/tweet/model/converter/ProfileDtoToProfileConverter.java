package com.bel.tweet.model.converter;

import com.bel.tweet.model.dto.ProfileDto;
import com.bel.tweet.model.entity.Profile;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class ProfileDtoToProfileConverter implements Converter<ProfileDto, Profile> {

    private final ModelMapper modelMapper;

    @Override
    public Profile convert(ProfileDto source) {
        return modelMapper.map(source, Profile.class);
    }
}
