package com.bel.tweet.model.converter;

import com.bel.tweet.model.dto.UserDto;
import com.bel.tweet.model.entity.User;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class UserToUserDtoConverter implements Converter<User, UserDto> {

    private final ModelMapper modelMapper;

    @Override
    public UserDto convert(User source) {
        return modelMapper.map(source, UserDto.class);
    }
}
