package com.bel.tweet.model.converter;

import com.bel.tweet.model.dto.PostRatingDto;
import com.bel.tweet.model.entity.PostRating;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class PostRatingDtoToPostRatingConverter implements Converter<PostRatingDto, PostRating> {

    private final ModelMapper modelMapper;

    @Override
    public PostRating convert(PostRatingDto source) {
        return modelMapper.map(source, PostRating.class);
    }
}
