package com.bel.tweet.model.converter;

import com.bel.tweet.model.dto.RoleDto;
import com.bel.tweet.model.entity.Role;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class RoleDtoToRoleConverter implements Converter<RoleDto, Role> {

    private final ModelMapper modelMapper;

    @Override
    public Role convert(RoleDto source) {
        return modelMapper.map(source, Role.class);
    }
}
