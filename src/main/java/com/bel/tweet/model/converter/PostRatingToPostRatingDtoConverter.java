package com.bel.tweet.model.converter;

import com.bel.tweet.model.dto.PostRatingDto;
import com.bel.tweet.model.entity.PostRating;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class PostRatingToPostRatingDtoConverter implements Converter<PostRating, PostRatingDto> {

    private final ModelMapper modelMapper;

    @Override
    public PostRatingDto convert(PostRating source) {
        return modelMapper.map(source, PostRatingDto.class);
    }
}
