package com.bel.tweet.model.converter;

import com.bel.tweet.model.enumeration.Rating;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class StringToRatingConverter implements Converter<String, Rating> {

    @Override
    public Rating convert(String source) {
        return Rating.valueOf(source.toUpperCase(Locale.ROOT));
    }
}
