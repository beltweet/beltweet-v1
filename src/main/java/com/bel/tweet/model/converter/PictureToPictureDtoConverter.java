package com.bel.tweet.model.converter;

import com.bel.tweet.model.dto.PictureDto;
import com.bel.tweet.model.entity.Picture;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class PictureToPictureDtoConverter implements Converter<Picture, PictureDto> {

    private final ModelMapper modelMapper;

    @Override
    public PictureDto convert(Picture source) {
        return modelMapper.map(source, PictureDto.class);
    }
}
