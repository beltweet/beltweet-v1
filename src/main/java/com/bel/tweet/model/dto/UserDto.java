package com.bel.tweet.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.bel.tweet.model.dto.util.DtoProperties.DATE_PATTERN;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String id;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String username;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String email;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String firstName;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String lastName;

    @JsonProperty(
            access = JsonProperty.Access.READ_ONLY,
            value = "enabled")
    private boolean isEnabled;

    @JsonProperty(
            access = JsonProperty.Access.READ_ONLY,
            value = "accountNonExpired")
    private boolean isAccountNonExpired;

    @JsonProperty(
            access = JsonProperty.Access.READ_ONLY,
            value = "accountNonLocked")
    private boolean isAccountNonLocked;

    @JsonProperty(
            access = JsonProperty.Access.READ_ONLY,
            value = "credentialsNonExpired")
    private boolean isCredentialsNonExpired;

    @Builder.Default
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<RoleDto> roles = new ArrayList<>();

    @Builder.Default
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = DATE_PATTERN)
    private LocalDateTime passedIn = LocalDateTime.now();
}
