package com.bel.tweet.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserInformationDto {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private UUID id;

    private CurrentLocationDto currentLocation;

    private String description;

    @JsonProperty(
            access = JsonProperty.Access.READ_ONLY,
            value = "empty")
    private boolean isEmpty;
}
