package com.bel.tweet.model.dto.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class DtoProperties {
    public static final String DATE_PATTERN = "yyyy-MM-dd HH:mm";
}
