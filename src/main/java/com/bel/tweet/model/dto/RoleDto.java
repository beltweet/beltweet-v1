package com.bel.tweet.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RoleDto {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    int id;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    String name;
}
