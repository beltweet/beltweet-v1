package com.bel.tweet.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.time.LocalDateTime;

import static com.bel.tweet.model.dto.util.DtoProperties.DATE_PATTERN;

@Builder
public class ExceptionDto {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String exception;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String message;

    @Builder.Default
    @JsonFormat(pattern = DATE_PATTERN)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private LocalDateTime timestamp = LocalDateTime.now();
}
