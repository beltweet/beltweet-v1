package com.bel.tweet.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PostDto {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private UUID id;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private UUID userOwnerId;

    //@Min(value = 10)
    private String description;

    @Builder.Default
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Map<String, Integer> ratings = new HashMap<>();

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private LocalDateTime postedIn;
}
