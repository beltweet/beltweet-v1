package com.bel.tweet.model.enumeration;

public enum Rating {
    LIKE,
    DISLIKE
}
