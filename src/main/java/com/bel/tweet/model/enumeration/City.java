package com.bel.tweet.model.enumeration;

public enum City {
    NONE,
    MINSK,
    MOSCOW,
    KIEV,
    ASTANA,
    BUCHAREST,
    VILNUS,
    WASHINGTON,
    KAIR,
    PEKIN,
    SEOUL
}
