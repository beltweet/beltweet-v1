package com.bel.tweet.model.enumeration;

public enum Country {
    NONE,
    BELARUS,
    RUSSIA,
    UKRAINE,
    KAZAKHSTAN,
    ROMANIA,
    LITHUANIA,
    USA,
    EGYPT,
    SOUTH_KOREA
}
