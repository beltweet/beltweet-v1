package com.bel.tweet.service;

import com.amazonaws.HttpMethod;
import com.bel.tweet.model.entity.Picture;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;
//todo add in factories type of a file
public interface FileService {

    void upload(MultipartFile multipartFile, String userId) throws IOException;

    Picture download(String userId, HttpMethod httpMethod);

    void upload(MultipartFile[] multipartFiles, String postId, String userId) throws IOException;

    default String fileNameFactory(String fileName) {
        return String.format("%s-%s", fileName, UUID.randomUUID());
    }

    default String pathFactory(String bucketName, long profileId) {
        return String.format("%s/%d", bucketName, profileId);
    }

    default String keyFactory(long profileId, String fileName) {
        return String.format("%d/%s", profileId, fileName);
    }
}
