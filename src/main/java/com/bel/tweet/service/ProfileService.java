package com.bel.tweet.service;

import com.bel.tweet.model.entity.Picture;
import com.bel.tweet.model.entity.Profile;
import com.bel.tweet.model.entity.User;
import com.bel.tweet.model.enumeration.Country;

public interface ProfileService {

    void create(User user);

    Profile findProfileById(long id);
    //todo delete
    Profile findProfileByUserId(String id, Profile profile);

    Country findProfileCountryByUserId(String id);

    void update(String id, Profile profile);

    void update(String id, String fileName, String key);

    long findProfileIdByUserId(String id);

    String findPictureIdByProfileId(long id);

    Picture findPictureByUserId(String id);

    boolean findIfPictureIsDefaultByUserId(String id);

    void save(String id, String fileName, String path);
}
