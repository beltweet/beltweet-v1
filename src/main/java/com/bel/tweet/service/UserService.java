package com.bel.tweet.service;

import com.bel.tweet.model.entity.User;

public interface UserService {

    User findUserById(String id);

    User findUserByUsername(String username);
}
