package com.bel.tweet.service;

import com.bel.tweet.model.entity.Post;
import com.bel.tweet.model.entity.PostRating;

import java.util.List;

public interface PostService {

    void create(Post post);

    void rate(PostRating postRating);

    List<Post> getAllPostsByUserId(String userId);
}
