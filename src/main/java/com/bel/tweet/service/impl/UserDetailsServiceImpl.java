package com.bel.tweet.service.impl;

import com.bel.tweet.exception.EntityNotFoundException;
import com.bel.tweet.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = userService.findUserByUsername(username);

        if(user == null)
            throw new EntityNotFoundException(username);

        return User.builder()
                .username(user.getUsername())
                .password(user.getPassword())
                .disabled(user.isEnabled())
                .accountExpired(user.isAccountNonExpired())
                .accountLocked(user.isAccountNonLocked())
                .authorities(user.getAuthorities())
                .build();
    }
}
