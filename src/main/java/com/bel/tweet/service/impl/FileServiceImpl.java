package com.bel.tweet.service.impl;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.bel.tweet.config.util.AwsProperties;
import com.bel.tweet.exception.TooManyFilesException;
import com.bel.tweet.model.entity.Picture;
import com.bel.tweet.service.FileService;
import com.bel.tweet.service.PostService;
import com.bel.tweet.service.ProfileService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.impl.InvalidContentTypeException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static org.apache.http.entity.ContentType.IMAGE_JPEG;
import static org.apache.http.entity.ContentType.IMAGE_PNG;

@Slf4j
@Service
@AllArgsConstructor
public class FileServiceImpl implements FileService {

    private final AwsProperties awsProperties;
    private final AmazonS3 amazonS3;
    private final ProfileService profileService;
    private final PostService postService;

    @Override
    public void upload(@NonNull MultipartFile multipartFile, String userId) throws IOException {
        var contentType = multipartFile.getContentType();

        assert contentType != null;

        if(contentType.equals(IMAGE_PNG.getMimeType()) && contentType.equals(IMAGE_JPEG.getMimeType()))
            throw new InvalidContentTypeException("Content type must be png or jpeg");

        Map<String, String> metadata = new HashMap<>();

        metadata.put("Content-Type", contentType);
        metadata.put("Content-Length", String.valueOf(multipartFile.getSize()));

        var fileName = fileNameFactory(multipartFile.getName());

        var profileId = profileService.findProfileIdByUserId(userId);

        var pictureIsDefault = profileService.findIfPictureIsDefaultByUserId(userId);

        var path = pathFactory(awsProperties.getBucketName(), profileId);

        var key = keyFactory(profileId, fileName);

        if(!pictureIsDefault) {
            var pictureId = profileService.findPictureIdByProfileId(profileId);

            profileService.update(pictureId, fileName, key);
        } else
            profileService.save(userId, fileName, key);

        uploadToBucketS3(fileName, path, metadata, multipartFile.getInputStream());
    }

    @Override
    public Picture download(String userId, HttpMethod httpMethod) {
        var picture = profileService.findPictureByUserId(userId);

        var calendar = Calendar.getInstance();

        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR, 1);

        var key = picture.getPath();

        picture.setName(amazonS3.generatePresignedUrl(
                        awsProperties.getBucketName(), key, calendar.getTime(), httpMethod).toString());

        return picture;
    }
    //todo
    @Override
    public void upload(MultipartFile[] multipartFiles, String postId, String userId) {
        Map<String, String> metadata = new HashMap<>();

        if(multipartFiles.length > 10)
            throw new TooManyFilesException("Amount of files must be under or equals 10");

        Arrays.stream(multipartFiles).filter(file -> {
            var contentType = file.getContentType();

            assert contentType != null;

            return contentType.equals(IMAGE_PNG.getMimeType()) && contentType.equals(IMAGE_JPEG.getMimeType());
        }).forEach(file -> {
            metadata.put("Content-Type", file.getContentType());
            metadata.put("Content-Length", String.valueOf(file.getSize()));

            var fileName = fileNameFactory(file.getName());

            var profileId = profileService.findProfileIdByUserId(userId);

            var path = pathFactory(awsProperties.getBucketName(), profileId);

            var key = keyFactory(profileId, fileName);

            try {
                uploadToBucketS3(fileName, path, metadata, file.getInputStream());
            } catch (IOException e) {
                log.warn("Nested exception: {}", e.getMessage());
            }
        });
    }

    private void uploadToBucketS3(String fileName, String path, Map<String, String> metadata, InputStream inputStream) {
        var objectMetadata = new ObjectMetadata();

        try {
            metadata.forEach(objectMetadata::addUserMetadata);

            amazonS3.putObject(path, fileName, inputStream, objectMetadata);
        } catch (AmazonServiceException e) {
            log.warn("Nested exception: {}", e.getMessage());
        }
    }
}
