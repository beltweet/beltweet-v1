package com.bel.tweet.service.impl;

import com.bel.tweet.dao.ProfileDao;
import com.bel.tweet.model.entity.Picture;
import com.bel.tweet.model.entity.Profile;
import com.bel.tweet.model.entity.User;
import com.bel.tweet.model.enumeration.Country;
import com.bel.tweet.service.ProfileService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class ProfileServiceImpl implements ProfileService {

    private final ProfileDao profileDao;

    @Override
    @Transactional
    public void create(User user) {
        var profile = Profile.builder()
                .user(user)
                .build();

        profileDao.save(profile);
    }

    @Override
    @Transactional
    public Profile findProfileById(long id) {
        return profileDao.findProfileById(id);
    }

    @Override
    @Transactional
    public Profile findProfileByUserId(String id, Profile profile) {
        return profileDao.findProfileByUserId(id, profile);
    }

    @Override
    @Transactional
    public Country findProfileCountryByUserId(String id) {
        return profileDao.findProfileCountryByUserId(id);
    }

    @Override
    @Transactional
    public void update(String id, Profile profile) {
        var updatedProfile = profileDao.findProfileByUserId(id, profile);

        profileDao.update(updatedProfile);
    }

    @Override
    @Transactional
    public void update(String id, String fileName, String key) {
        profileDao.update(id, fileName, key);
    }

    @Override
    @Transactional
    public long findProfileIdByUserId(String id) {
        return profileDao.findProfileIdByUserId(id);
    }

    @Override
    public String findPictureIdByProfileId(long id) {
        return profileDao.findPictureIdByProfileId(id);
    }

    @Override
    @Transactional
    public Picture findPictureByUserId(String id) {
        return profileDao.findPictureByUserId(id);
    }

    @Override
    public boolean findIfPictureIsDefaultByUserId(String id) {
        return profileDao.findIfPictureIsDefaultByUserId(id);
    }

    @Override
    public void save(String id, String fileName, String path) {
        profileDao.save(id, fileName, path);
    }
}
