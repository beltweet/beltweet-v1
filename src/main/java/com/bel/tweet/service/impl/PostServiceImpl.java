package com.bel.tweet.service.impl;

import com.bel.tweet.dao.PostDao;
import com.bel.tweet.dao.PostRatingDao;
import com.bel.tweet.model.entity.Post;
import com.bel.tweet.model.entity.PostRating;
import com.bel.tweet.model.enumeration.Rating;
import com.bel.tweet.service.PostService;
import com.bel.tweet.service.ProfileService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
@AllArgsConstructor
public class PostServiceImpl implements PostService {

    private final PostDao postDao;
    private final PostRatingDao postRatingDao;
    private final ProfileService profileService;

    @Override
    public void create(Post post) {
        var ownerId = post.getUserOwnerId().toString();

        var country = profileService.findProfileCountryByUserId(ownerId);

        post.setCountryOfOwner(country);
        post.setPostedIn(LocalDateTime.now());

        postDao.save(post);
    }

    @Override
    public void rate(PostRating postRating) {
        postRatingDao.save(postRating);
    }

    @Override
    public List<Post> getAllPostsByUserId(String userId) {
        var userUUID = UUID.fromString(userId);

        var posts =  postDao.findAllPostsByUserId(userUUID);

        posts.forEach(post -> {
            var postId = post.getId().toString();

            var likes = postRatingDao.findCountedRatingByRatingName(Rating.LIKE.name(), postId);

            var dislikes = postRatingDao.findCountedRatingByRatingName(Rating.DISLIKE.name(), postId);

            Map<String, Integer> rating = new HashMap<>();

            rating.put("likes", likes);
            rating.put("dislikes", dislikes);

            post.setRatings(rating);
        });

        return posts;
    }
}
