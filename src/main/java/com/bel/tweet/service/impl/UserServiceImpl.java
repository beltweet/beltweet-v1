package com.bel.tweet.service.impl;

import com.bel.tweet.dao.UserDao;
import com.bel.tweet.model.entity.User;
import com.bel.tweet.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserDao userDao;

    @Override
    @Transactional
    public User findUserById(String id) {
        return userDao.findUserById(id);
    }

    @Override
    @Transactional
    public User findUserByUsername(String username) {
        return userDao.findUserByUsername(username);
    }


}
