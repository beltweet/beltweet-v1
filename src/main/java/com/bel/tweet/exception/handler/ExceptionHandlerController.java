package com.bel.tweet.exception.handler;

import com.bel.tweet.exception.EntityNotFoundException;
import com.bel.tweet.exception.TooManyFilesException;
import com.bel.tweet.model.dto.ExceptionDto;
import lombok.AllArgsConstructor;
import org.apache.tomcat.util.http.fileupload.impl.InvalidContentTypeException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@AllArgsConstructor
public class ExceptionHandlerController<T extends RuntimeException> {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ExceptionDto> entityNotFoundExceptionHandler(T e) {
        return new ResponseEntity<>(exceptionFactory(e), HttpStatus.BAD_REQUEST);
    }
    //todo change exception
    @ExceptionHandler(InvalidContentTypeException.class)
    public ResponseEntity<ExceptionDto> invalidContentTypeExceptionHandler(InvalidContentTypeException e) {
        var eDto = ExceptionDto.builder()
                .exception(e.getClass().getName())
                .message(e.getMessage())
                .build();

        return new ResponseEntity<>(eDto, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(TooManyFilesException.class)
    public ResponseEntity<ExceptionDto> tooManyFilesExceptionHandler(T e) {
        return new ResponseEntity<>(exceptionFactory(e), HttpStatus.BAD_REQUEST);
    }

    private ExceptionDto exceptionFactory(T e) {
        return ExceptionDto.builder()
                .exception(e.getClass().getName())
                .message(e.getMessage())
                .build();
    }
}
