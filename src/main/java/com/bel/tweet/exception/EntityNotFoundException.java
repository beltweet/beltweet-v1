package com.bel.tweet.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EntityNotFoundException extends RuntimeException {

    public static final String ENTITY_NOT_FOUND = "Entity with principal %s not found.";

    public EntityNotFoundException(String entityPrincipal) {
        super(String.format(ENTITY_NOT_FOUND, entityPrincipal));
    }
}
