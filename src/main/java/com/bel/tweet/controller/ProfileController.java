package com.bel.tweet.controller;

import com.bel.tweet.config.util.RoleExpression;
import com.bel.tweet.controller.util.Endpoints;
import com.bel.tweet.model.dto.ProfileDto;
import com.bel.tweet.model.entity.Profile;
import com.bel.tweet.service.ProfileService;
import com.bel.tweet.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;

@RestController
@RequestMapping(Endpoints.ProfileControllerEndpoints.PROFILE_MAPPING)
@AllArgsConstructor
public class ProfileController {

    private ConversionService conversionService;
    private UserService userService;
    private ProfileService profileService;

    @PreAuthorize(RoleExpression.PERMIT_ALL)
    @GetMapping(Endpoints.ProfileControllerEndpoints.CREATE_PERSONAL_PROFILE) //todo use userId instead of user entity
    public ResponseEntity<Void> createPersonalProfile(@AuthenticationPrincipal Principal principal) {
        var user = userService.findUserById(principal.getName());

        profileService.create(user);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PreAuthorize(RoleExpression.PERMIT_ALL)
    @GetMapping(Endpoints.ProfileControllerEndpoints.PERSONAL_PROFILE)
    public ResponseEntity<ProfileDto> getPersonalProfile(@PathVariable Long profileId) {

        return new ResponseEntity<>(conversionService
                .convert(profileService.findProfileById(profileId), ProfileDto.class), HttpStatus.OK);
    }

    @PreAuthorize(RoleExpression.AUTHENTICATED)
    @PutMapping(Endpoints.ProfileControllerEndpoints.UPDATE_PERSONAL_PROFILE)
    public ResponseEntity<Void> updatePersonalProfile(@AuthenticationPrincipal Principal principal,
                                                      @RequestBody ProfileDto profileDto) {
        var userId = principal.getName();

        profileService.update(userId, conversionService.convert(profileDto, Profile.class));

        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
