package com.bel.tweet.controller;

import com.bel.tweet.config.util.RoleExpression;
import com.bel.tweet.controller.util.Endpoints;
import com.bel.tweet.model.dto.PostDto;
import com.bel.tweet.model.dto.PostRatingDto;
import com.bel.tweet.model.entity.Post;
import com.bel.tweet.model.entity.PostRating;
import com.bel.tweet.model.enumeration.Rating;
import com.bel.tweet.service.FileService;
import com.bel.tweet.service.PostService;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping(Endpoints.PostControllerEndpoints.POST_MAPPING)
@AllArgsConstructor
public class PostController {

    private final ConversionService conversionService;
    private final PostService postService;

    @PreAuthorize(RoleExpression.AUTHENTICATED)
    @PostMapping(Endpoints.PostControllerEndpoints.CREATE_POST)
    public ResponseEntity<Void> createPost(@AuthenticationPrincipal Principal principal, @Valid @RequestBody PostDto postDto) {
        var userId = UUID.fromString(principal.getName());

        postDto.setUserOwnerId(userId);

        postService.create(conversionService.convert(postDto, Post.class));

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PreAuthorize(RoleExpression.PERMIT_ALL)
    @GetMapping(Endpoints.PostControllerEndpoints.VIEW_POSTS)
    public ResponseEntity<List<PostDto>> viewPosts(@PathVariable String userId) {
        return new ResponseEntity<>(
                postService.getAllPostsByUserId(userId).stream()
                .map(s -> conversionService.convert(s, PostDto.class))
                .collect(Collectors.toList()), HttpStatus.OK);
    }

    @PreAuthorize(RoleExpression.AUTHENTICATED)
    @GetMapping(Endpoints.PostControllerEndpoints.RATE_POST)
    public ResponseEntity<Void> ratePost(@AuthenticationPrincipal Principal principal,
                                         @PathVariable String postId,
                                         @PathVariable Rating rating) {
        var userId = principal.getName();

        var postRatingDto = PostRatingDto.builder()
                .postId(postId)
                .userId(userId)
                .rating(rating)
                .build();

        postService.rate(conversionService.convert(postRatingDto, PostRating.class));

        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
