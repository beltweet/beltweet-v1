package com.bel.tweet.controller;

import com.bel.tweet.controller.util.Endpoints;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(Endpoints.UserControllerEndpoints.USER_MAPPING)
@AllArgsConstructor
public class UserController {
    private final ConversionService conversionService;
}
