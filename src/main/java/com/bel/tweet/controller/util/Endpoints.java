package com.bel.tweet.controller.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Endpoints {

    public static final String ROOT_URL = "http://localhost:3030"; //do something with port

    @UtilityClass
    public static class UserControllerEndpoints {
        public static final String USER_MAPPING = "/api/v1/user";
        public static final String LOGOUT = "/logout";
        public static final String SIGN_UP = "/sign-up";
        public static final String SIGN_IN = "/sign-in";
    }

    @UtilityClass
    public static class ProfileControllerEndpoints {
        public static final String PROFILE_MAPPING = "/api/v1/user/profile";
        public static final String PERSONAL_PROFILE = "/{profileId}";
        public static final String CREATE_PERSONAL_PROFILE = "/create";
        public static final String UPDATE_PERSONAL_PROFILE = "/update";
    }

    @UtilityClass
    public static class HubControllerEndpoints {
        public static final String HUB_MAPPING = "/api/v1";
        public static final String HUB = "/hub";
    }

    @UtilityClass
    public static class PostControllerEndpoints {
        public static final String POST_MAPPING = "/api/v1/post";
        public static final String CREATE_POST = "/create";
        public static final String RATE_POST = "/{postId}/{rating}";
        public static final String VIEW_POSTS = "/{userId}";
    }

    @UtilityClass
    public static class FileControllerEndpoints {
        public static final String FILE_MAPPING = "/api/v1/storage";
        public static final String UPLOAD_PROFILE_IMAGE = "/upload";
        public static final String DOWNLOAD_FILE = "/download";
        public static final String UPLOAD_POST_FILE = "/post/{postId}/upload";
    }
}
