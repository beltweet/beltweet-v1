package com.bel.tweet.controller;

import com.bel.tweet.controller.util.Endpoints;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Endpoints.HubControllerEndpoints.HUB_MAPPING)
@AllArgsConstructor
public class HubController {
}
