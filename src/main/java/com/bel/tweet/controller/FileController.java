package com.bel.tweet.controller;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.HttpMethod;
import com.bel.tweet.config.util.RoleExpression;
import com.bel.tweet.controller.util.Endpoints;
import com.bel.tweet.model.dto.PictureDto;
import com.bel.tweet.service.FileService;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;

@RestController
@RequestMapping(Endpoints.FileControllerEndpoints.FILE_MAPPING)
@AllArgsConstructor
public class FileController {

    private final FileService fileService;
    private final ConversionService conversionService;

    @PreAuthorize(RoleExpression.AUTHENTICATED)
    @PutMapping(
            path = Endpoints.FileControllerEndpoints.UPLOAD_PROFILE_IMAGE,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> uploadProfileImage(@RequestParam(value = "file", required = false) MultipartFile multipartFile,
                                           @AuthenticationPrincipal Principal principal) throws IOException {
        var userId = principal.getName();

        try {
            fileService.upload(multipartFile, userId);
        } catch (AmazonServiceException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PreAuthorize(RoleExpression.AUTHENTICATED)
    @GetMapping(Endpoints.FileControllerEndpoints.DOWNLOAD_FILE)
    public @ResponseBody PictureDto downloadProfileImage(@AuthenticationPrincipal Principal principal) {
        var userId = principal.getName();

        return conversionService.convert(fileService.download(userId, HttpMethod.GET), PictureDto.class);
    }

    //todo
    @PreAuthorize(RoleExpression.AUTHENTICATED)
    @PostMapping(
            path = Endpoints.FileControllerEndpoints.UPLOAD_POST_FILE,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> uploadPostFiles(@PathVariable String postId,
                                               @RequestParam(value = "files", required = false) MultipartFile[] multipartFiles,
                                               @AuthenticationPrincipal Principal principal) {

        var userId = principal.getName();

        try {
            fileService.upload(multipartFiles, postId, userId);
        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
