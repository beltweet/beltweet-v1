package com.bel.tweet.config.util;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "aws.s3")
@Data
@Accessors(chain = true)
public class AwsProperties {

    private String accessKeyId;
    private String clientId;
    private String region;
    private String bucketName;
    private String defaultImagePath;
    private String defaultImageName;
}
