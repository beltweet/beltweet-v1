package com.bel.tweet.config.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class RoleExpression {

    public static final String PERMIT_ALL = "permitAll()";
    public static final String AUTHENTICATED = "isAuthenticated()";
    public static final String ROLE_ADMIN = "hasRole('ADMIN')";
    public static final String ROLE_USER = "hasRole('USER')";
}