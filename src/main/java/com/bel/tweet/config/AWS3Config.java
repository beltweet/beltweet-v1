package com.bel.tweet.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.bel.tweet.config.util.AwsProperties;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
public class AWS3Config {

    private final AwsProperties awsProperties;

    @Bean
    public AmazonS3 amazonS3() {
        var awsCredentials = new BasicAWSCredentials(awsProperties.getAccessKeyId(), awsProperties.getClientId());

        return AmazonS3ClientBuilder.standard()
                .withRegion(awsProperties.getRegion())
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
                .build();
    }
}
